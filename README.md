# Bitwarden File Transfer Tool
A tool to store files in Bitwarden

# SYNOPSIS
     usage: [ upload <file> | download <file> | delete <file> | list ]

     Arguments:
       upload <file>      Upload a file to Bitwarden.
       download <file>    Download a file from Bitwarden.
       delete <file>      Delete a file from Bitwarden.
       list               List files in Bitwarden.

     Options:
       --help|-h          Brief help message.
       --verbose|-v       Verbose logging.

# DESCRIPTION
This program uses Bitwarden attachments to store files.

A placeholder item is used to hold the attachments.

Please make sure `bw` is installed and available in your `$PATH`,
and that you have logged in and unlocked it.

`bw` is the [Bitwarden CLI](https://bitwarden.com/help/cli/).


# SCREENSHOT

![example](./img/demo.svg)


# KNOWN LIMITATIONS

All files are currently stored in the same "directory" in Bitwarden, so you need to use unique filenames.


# AUTHOR
Written by Per Weijnitz.

# REPORTING BUGS
https://gitlab.com/arbetsformedlingen/devops/bitwarden-files


# COPYRIGHT
This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.
