#!/usr/bin/env bash
#
# File:           bitwarden_files
#
# Author:         Per Weijnitz
# E-Mail:         per.weijnitz@arbetsformedlingen.se
# Org:            arbetsformedlingen.se
# License:        GPLv3
#

IFS='' read -r -d '' HELPTEXT <<=cut
=head1 Bitwarden File Transfer Tool

 A tool to store files in Bitwarden

=head1 SYNOPSIS

 usage: [ upload <file> | download <file> | delete <file> | list ]

 Arguments:
   upload <file>      Upload a file to Bitwarden.
   download <file>    Download a file from Bitwarden.
   delete <file>      Delete a file from Bitwarden.
   list               List files in Bitwarden.

 Options:
   --help|-h          Brief help message.
   --verbose|-v       Verbose logging.


=head1 DESCRIPTION

  B<This program> uses Bitwarden attachments to store files.

  A placeholder item is used to hold the attachments.

  Please make sure C<bw> is installed and available in your C<$PATH>,
  and that you have logged in and unlocked it.

  C<bw> is the Bitwarden CLI.


=head1 AUTHOR

  Written by Per Weijnitz.


=head1 REPORTING BUGS

  L<https://gitlab.com/arbetsformedlingen/devops/bitwarden-files>


=head1 COPYRIGHT

  This is free software: you are free to change and redistribute it. There
  is NO WARRANTY, to the extent permitted by law.


=cut

set -euo pipefail
IFS=$'\n\t'


# A Bitwarden item used to attach files to.
# Will be created if needed.
attachment_placeholder_item_name=attachment_holder


# Verbose logging
verbose=0


if ! command -v jq >/dev/null \
    || ! command -v bw >/dev/null; then
    echo "install jq and bw" >&2; exit 1
fi


if [ $(bw status | jq -r .status) != "unlocked" ]; then
    echo "make sure bw is logged in and unlocked" >&2; exit 1
fi



function help() {
    echo "$HELPTEXT"
}


function log() {
    local line="${1:-undefined}"

    if [ "$verbose" = 1 ]; then
        echo "$line" >&2
    fi
}


# all attachments in Bitwarden needs an item to be attached to.
function create_root_item() {
    log "Creating root item."
    bw get template item | \
        jq '.type = 2 |
        .secureNote.type = 0 |
        .notes = "Placeholder item for attachments." |
        .name = "'"$attachment_placeholder_item_name"'"' | \
        bw encode | \
        bw create item |\
        jq -r .id
}


function init() {
    local id

    log "Checking for the placeholder item..."
    if ! id=$(bw get item "$attachment_placeholder_item_name" | jq -r .id); then
        id=$(create_root_item)
    fi
    echo $id
}



function upload() {
    local itemid="${1:-undefined}"
    local filename="${2:-undefined}"

    if [ ! -f "$filename" ]; then
        echo "file does not exist: $filename" >&2; exit 1
    fi

    local errfile=$(mktemp)
    if bw get attachment "$filename" --itemid "$itemid" --raw >/dev/null 2>"$errfile"; then
        echo "File already exists in Bitwarden: $filename" >&2; exit 1
    fi

    log "$(cat $errfile)"
    rm -f "$errfile"

    log "Uploading file $filename"
    bw create attachment --file "$filename" --itemid "$itemid" | jq -r .id
}



function download() {
    local itemid="${1:-undefined}"
    local filename="${2:-undefined}"

    if [ -f "$filename" ]; then
        echo "target file already exists: $filename" >&2; exit 1
    fi

    bw get attachment "$filename" --itemid "$itemid"
}



function delete() {
    local itemid="${1:-undefined}"
    local filename="${2:-undefined}"

    log "Attempting to find and delete $filename"
    if fileid=$(bw get item "$attachment_placeholder_item_name" | jq -r '.attachments[] | select(.fileName == "'"$filename"'") | .id'); then
        if [[ $fileid =~ ^[[:alnum:]]+$ ]]; then
            log "Deleting $filename"
            bw delete attachment "$fileid" --itemid "$itemid"
        else
            echo "Suspicious file id $fileid, exiting." >&2; exit 1
        fi
    else
        log "No file found named $filename"
    fi
}




#######################################################################
#### MAIN


operation=""
fname=""

while [ "$#" -gt 0 ]; do
    case "$1" in
        upload)       operation="upload"; fname="$2"; shift;;
        download)     operation="download"; fname="$2"; shift;;
        delete)       operation="delete"; fname="$2"; shift;;
        list)         operation="list";;
        --verbose|-v) verbose=1;;
        --help|-h)    help; exit;;
        --)           shift; break;;
        *)            echo "unknown opt $1, try $0 -h" >&2; exit 1;;
    esac
    shift
done


itemid=$(init)


case "$operation" in
    upload)    upload   "$itemid" "$fname";;
    download)  download "$itemid" "$fname";;
    delete)    delete "$itemid" "$fname";;
    list)      bw get item "$attachment_placeholder_item_name" \
                | jq -r '.attachments[] | .fileName' 2>/dev/null \
                || echo "No files found" >&2;;
    *)         echo "unknown operation $operation" >&2; exit 1;;
esac
