#!/bin/bash
###############################################################################
#
# Script for keeping a modular ssh config in synk with a safe storage in
# Bitwarden.
#
# Usage:
#
#     bitwarden-ssh-sync.sh [ upload | download ]
#
#   Make sure you are logged in and unlocked in bw, tbe Bitwarden CLI.
#
#
# Description:
#
#   The program allows for uploading and downloading of specific files
#   between your Bitwarden vault and your local file system.

#   To use it, you must follow the file and directory structure
#   described in the background section below.
#
#
# Prerequisites:
#
#   Install these programs and keep them in your PATH:
#    - bw from https://bitwarden.com/help/cli/
#    - bitwarden-files.sh from https://gitlab.com/arbetsformedlingen/devops/bitwarden-file-tool
#
#
# Background:
#
#   I use a modular ssh config, with this content in ~/.ssh/config:
#
#     include config.d.priv/*
#
#   and in config.d.priv/ I store pairs of single host config files and a
#   subdirectory with its keys. For example, here is the config set for
#   Gitlab:
#
#     config.d.priv/gitlab
#     config.d.priv/gitlab.keys/id_gitlab
#     config.d.priv/gitlab.keys/id_gitlab.pub
#
#   and the contents of config.d.priv/gitlab is:
#
#     Host gitlab.com
#         IdentityFile ~/.ssh/config.d.priv/gitlab.keys/id_gitlab
#
#
set -euo pipefail
IFS=$'\n\t'


SSHCONFDIR=~/.ssh/config.d.priv
STORAGEPREFIX=sshconf_priv

# cli args
mode="${1:-}"


# init checks
if [ "$mode" != "upload" ] &&  [ "$mode" != "download" ]; then
    echo "**** error: choose mode upload or download" >&2
    exit 1
fi
if [ $(bw status | jq -r .status) != "unlocked" ]; then
    echo "**** error: bw should be logged in and unlocked" >&2
    exit 1
fi
command -v bitwarden-files.sh >/dev/null || { echo "**** error: cannot find bitwarden-files.sh" >&2; exit 1; }


# paranoid tmpdir tricks to avoid bad deletion on batch rm -rf *
TMPDBASE=$(mktemp -d)
TMPD="$TMPDBASE".tmpd
mkdir -p "$TMPD"


# clean up
trap "rm -rf $TMPDBASE.tmpd" EXIT


# main
mkdir -p "$SSHCONFDIR"

if [ "$mode" = "upload" ]; then
    for CONF in $(find "$SSHCONFDIR"/ -maxdepth 1 -type f); do
	mkdir -p "$TMPD"/local
	cp -r "$CONF" "$CONF.keys" "$TMPD"/local/
	cd "$TMPD"
	( cd local; tar -zcf ../"$STORAGEPREFIX"_$(basename "$CONF").tar.gz *; )

	do_upload=1
	if bitwarden-files.sh list | grep -q "^$STORAGEPREFIX"_$(basename "$CONF").tar.gz'$'; then
	    do_upload=0
	    mkdir -p remote_arc remote
	    ( cd remote_arc; bitwarden-files.sh download "$STORAGEPREFIX"_$(basename "$CONF").tar.gz; )
	    tar -C remote -xf remote_arc/"$STORAGEPREFIX"_$(basename "$CONF").tar.gz
	    if ! diff -r local remote; then
		echo $(basename "$CONF")" already exists on remote and differs" >&2
		if [[ "$(read -e -p 'Overwrite remote? [y/N]> '; echo $REPLY)" == [Yy]* ]]; then
		    echo "ok, overwriting" >&2
		    do_upload=1
		fi
	    fi
	fi

	if [ "$do_upload" = 1 ]; then
	    echo bitwarden-files.sh upload "$STORAGEPREFIX"_$(basename "$CONF").tar.gz >&2
	    bitwarden-files.sh upload "$STORAGEPREFIX"_$(basename "$CONF").tar.gz
	fi
	rm -rf "$TMPDBASE".tmpd/*
	cd - >/dev/null
    done
else
    for F in $(bitwarden-files.sh list | grep "^$STORAGEPREFIX"_); do
	cd "$TMPD"
	bitwarden-files.sh download "$F"
	archive=$(ls -1)
	tar -xf "$archive"
	rm -f "$archive"
	for file in $(find . -type f); do
	    do_write=1
	    if [ -f "$SSHCONFDIR"/"$file" ]; then
		do_write=0
		if ! diff "$file" "$SSHCONFDIR"/"$file"; then
		    echo "Remote version of $SSHCONFDIR/$file differs from local." >&2
		    if [[ "$(read -e -p 'Overwrite local? [y/N]> '; echo $REPLY)" == [Yy]* ]]; then
			do_write=1
		    else
			echo ok, skipping overwrite >&2
		    fi
		fi
	    fi
	    if [ "$do_write" = 1 ]; then
		echo cp "$file" "$SSHCONFDIR"/"$file" >&2
		cp "$file" "$SSHCONFDIR"/"$file"
	    fi
	done
	rm -rf "$TMPDBASE".tmpd/*
	cd - >/dev/null
    done
fi
